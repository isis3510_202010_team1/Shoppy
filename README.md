# Shoppy Flutter

Application made by Team #1 in the Mobile App Development course.


## Credentials and other useful testing stuff
For testing purposes, we provided the app with some testing credentials, for example:
 - e-mail: test@test.com
 - password: password

But please feel free to create an account on our platform and use it as you like
Also, because the app can be used to buy things, we provide the option to pay 
with credit card. But we don't expect you to use your own credit card only to 
test the payment function on Shoppy, so we'll give you one of our developer's credit card
(just kidding, it's a stripe testing card)
 - number: 4242 4242 4242 4242
 - exp. date: whatever you like, in the future
 - ccv: any 3 digits!

you can also use 4444 4444 4444 5555 with the same exp. date and ccv if the 
first one doesn't work 

Here we have a nifty page with all the QR Codes for all the products in out 
platform, divided by each store for your testing convenience.

[QR Codes](https://gitlab.com/isis3510_202010_team1/wiki/-/wikis/QR-Codes-for-the-testing-products)

So, now for the fun part: The APKs!

In the repo files there's a folder ./APKS.
Here you can find 4 APKs, 3 for each ABI (ARM, ARM64, x86_64) and a FAT APK that contains all versions
If none of the first three work, you might want to try the FAT APK
