import 'dart:async';
import 'package:path/path.dart';
import 'package:shoppy/models/store_model.dart';
import 'package:shoppy/models/user_model.dart';
import 'package:shoppy/models/transaction_model.dart' as Model;
import 'package:sqflite/sqflite.dart';
import '../models/cart_product_model.dart';

class DBProvider {
  DBProvider._();
  static final DBProvider db = DBProvider._();

  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await initDB();
    return _database;
  }

  initDB() async {
    String path = join(await getDatabasesPath(), 'shoppy.db');
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      print("INITIALIZING DBBBBBSSSSS");
      await db.execute("CREATE TABLE ShoppingListProducts ("
          "_id TEXT PRIMARY KEY,"
          "name TEXT,"
          "price INTEGER,"
          "store_id INTEGER,"
          "sku TEXT,"
          "img_url TEXT,"
          "description TEXT,"
          "amount INTEGER"
          ")");

      // StoreProducts
      await db.execute("CREATE TABLE StoreProducts ("
          "_id TEXT PRIMARY KEY,"
          "name TEXT,"
          "price INTEGER,"
          "store_id INTEGER,"
          "sku TEXT,"
          "img_url TEXT,"
          "description TEXT,"
          "amount INTEGER"
          ")");

      //VirtualCartProducts
      await db.execute("CREATE TABLE VirtualCartProducts ("
          "_id TEXT PRIMARY KEY,"
          "name TEXT,"
          "price INTEGER,"
          "store_id INTEGER,"
          "sku TEXT,"
          "img_url TEXT,"
          "description TEXT,"
          "amount INTEGER"
          ")");

      //Stores
      await db.execute("CREATE TABLE Stores ("
          "id INTEGER PRIMARY KEY,"
          "name TEXT,"
          "phone INTEGER,"
          "city TEXT,"
          "address TEXT,"
          "loclon REAL,"
          "loclat REAL,"
          "logoimg TEXT,"
          "currentusers INTEGER"
          ")");

      //Users
      await db.execute("CREATE TABLE Users ("
          "_id TEXT PRIMARY KEY,"
          "name TEXT,"
          "phone TEXT,"
          "gender TEXT,"
          "email TEXT,"
          "date_of_birth TEXT"
          ")");

      //Transactions
      await db.execute("CREATE TABLE Transactions ("
          "_id TEXT PRIMARY KEY,"
          "user_id TEXT,"
          "store_id INTEGER,"
          "payment_type TEXT,"
          "product_count INTEGER,"
          "total INTEGER,"
          "date TEXT"
          ")"
      );

      //Products From Transactions
      await db.execute("CREATE TABLE TransactionProducts ("
      "_id TEXT PRIMARY KEY,"
      "idtransaction TEXT,"
      "name TEXT,"
      "price INTEGER,"
      "store_id INTEGER,"
      "sku TEXT,"
      "img_url TEXT,"
      "description TEXT,"
      "amount INTEGER"
      ")");
    });
      
  }

  //-----------------------------
  //STORE PRODUCTS LIST
  //-----------------------------

  addStoreProduct(CartProduct newProduct) async {
    newProduct.amount = 1;
    // print("DB StoreProduct: Add:" + newProduct.id);
    final db = await database;
    // print(newProduct.id);
    await db.insert(
      'StoreProducts',
      newProduct.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<CartProduct> getStoreProduct(String id) async {
    final db = await database;
    var res =
        await db.query('StoreProducts', where: "_id = ?", whereArgs: [id]);
    return res.isNotEmpty ? CartProduct.fromMap(res.first) : null;
  }

  Future<List<CartProduct>> getAllStoreProducts() async {
    final db = await database;
    try {
      var res = await db.query("StoreProducts");
      List<CartProduct> products =
          res.isNotEmpty ? res.map((c) => CartProduct.fromMap(c)).toList() : [];
      return products;
    } catch (Exception) {
      print("CREATING DB StoreProducts");
      // StoreProducts
      await db.execute("CREATE TABLE StoreProducts ("
          "_id TEXT PRIMARY KEY,"
          "name TEXT,"
          "price INTEGER,"
          "store_id INTEGER,"
          "sku TEXT,"
          "img_url TEXT,"
          "description TEXT,"
          "amount INTEGER"
          ")");
      var res = await db.query("StoreProducts");
      List<CartProduct> products =
          res.isNotEmpty ? res.map((c) => CartProduct.fromMap(c)).toList() : [];
      print(products);
      return products;
    }
  }

  deleteStoreProduct(String id) async {
    final db = await database;
    return db.delete("StoreProducts", where: "id = ?", whereArgs: [id]);
  }

  deleteAllStoreProducts() async {
    final db = await database;
    db.rawDelete("DELETE from StoreProducts");
  }

  delteAllTables() async {
    final db = await database;
    db.execute("DROP TABLE IF EXISTS StoreProducts");
    db.execute("DROP TABLE IF EXISTS CartProduct");
    db.execute("DROP TABLE IF EXISTS VirtualCartProducts");
    db.execute("DROP TABLE IF EXISTS Stores");
    db.execute("DROP TABLE IF EXISTS Users");
    initDB();
  }

  //-----------------------------
  //Virtual Cart Stuff
  //-----------------------------

  addVirtualCartProduct(CartProduct newProduct) async {
    print("DB VirtualCartProduct: Add:" + newProduct.id);
    final db = await database;
    print(newProduct.id);
    await db.insert(
      'VirtualCartProducts',
      newProduct.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  getVirtualCartProduct(String id) async {
    final db = await database;
    var res =
        await db.query('VirtualCartProducts', where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? CartProduct.fromMap(res.first) : null;
  }

  Future<List<CartProduct>> getAllVirtualCartProducts() async {
    final db = await database;
    try {
      var res = await db.query("VirtualCartProducts");
      List<CartProduct> products =
          res.isNotEmpty ? res.map((c) => CartProduct.fromMap(c)).toList() : [];
      print(products);
      return products;
    } catch (Exception) {
      print("CREATING DB VirtualCartProducts");
      // VirtualCartProducts
      await db.execute("CREATE TABLE VirtualCartProducts ("
          "_id TEXT PRIMARY KEY,"
          "name TEXT,"
          "price INTEGER,"
          "store_id INTEGER,"
          "sku TEXT,"
          "img_url TEXT,"
          "description TEXT,"
          "amount INTEGER"
          ")");
      var res = await db.query("VirtualCartProducts");
      List<CartProduct> products =
          res.isNotEmpty ? res.map((c) => CartProduct.fromMap(c)).toList() : [];
      print(products);
      return products;
    }
  }

  deleteVirtualCartProduct(String id) async {
    final db = await database;
    return db.delete("VirtualCartProducts", where: "_id = ?", whereArgs: [id]);
  }

  deleteAllVirtualCartProducts() async {
    final db = await database;
    db.rawDelete("DELETE from VirtualCartProducts");
  }

  //------------------
  // STORES
  //------------------

  addStore(StoreModel store) async {
    final db = await database;
    await db.insert(
      'Stores',
      store.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  updateStore(StoreModel store) async {
    final db = await database;
    await db.insert('Stores', store.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<StoreModel> getStore(int id) async {
    final db = await database;
    var res = await db.query('Stores', where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? StoreModel.fromMap(res.first) : null;
  }

  Future<List<StoreModel>> getAllStores() async {
    final db = await database;
    try {
      var res = await db.query("Stores");
      List<StoreModel> stores =
          res.isNotEmpty ? res.map((c) => StoreModel.fromMap(c)).toList() : [];
      return stores;
    } on Exception catch (e) {
      await db.execute("CREATE TABLE Stores ("
          "id INTEGER PRIMARY KEY,"
          "name TEXT,"
          "phone INTEGER,"
          "city TEXT,"
          "address TEXT,"
          "loclon REAL,"
          "loclat REAL,"
          "logoimg TEXT,"
          "currentusers INTEGER"
          ")");
      var res = await db.query("Stores");
      List<StoreModel> stores =
          res.isNotEmpty ? res.map((c) => StoreModel.fromMap(c)).toList() : [];
      return stores;
    }
  }

  deleteStore(int id) async {
    final db = await database;
    return db.delete("Stores", where: "_id = ?", whereArgs: [id]);
  }

/// Users

  addUser(UserModel user) async {
    final db = await database;
    await db.delete('Users');
    await db.insert(
      'Users',
      user.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  getUser(String id) async {
    final db = await database;
    var res =
    await db.query('Users', where: "_id = ?", whereArgs: [id]);
    return res.isNotEmpty ? UserModel.fromMap(res.first) : null;
  }

  ///Shopping List

  addProductToShoppingList(CartProduct product)async{
    final db = await database;
    await db.insert('ShoppingListProducts', product.toMap(), conflictAlgorithm: ConflictAlgorithm.replace);
  }

  getProductFromShoppingList(String id) async{
    final db = await database;
    var res = await db.query('ShoppingListProducts', where: "_id = ?", whereArgs: [id]);
    return res.isNotEmpty ? CartProduct.fromMap(res.first):null;
  }

  Future<List<CartProduct>> getShoppingList() async {
    final db = await database;
    try {
      var res = await db.query("ShoppingListProducts");
      List<CartProduct> shoppingList =
          res.isNotEmpty ? res.map((c) => CartProduct.fromMap(c)).toList() : [];
      return shoppingList ;
    } on Exception catch (e) {
      await db.execute("CREATE TABLE ShoppingListProducts ("
      "_id TEXT PRIMARY KEY,"
      "name TEXT,"
      "price INTEGER,"
      "store_id INTEGER,"
      "sku TEXT,"
      "img_url TEXT,"
      "description TEXT,"
      "amount INTEGER"
      ")");
      var res = await db.query("Stores");
      List<CartProduct> shoppingList =
          res.isNotEmpty ? res.map((c) => CartProduct.fromMap(c)).toList() : [];
      return shoppingList;
    }
  }

  deleteProductFromShoppingList(String id) async {
    final db = await database;
    return db.delete("ShoppingListProducts", where: "_id = ?", whereArgs: [id]);
  }

  deleteAllProductsFromShoppingList() async {
    final db = await database;
    db.rawDelete("DELETE from ShoppingListProducts");
  }

  //Transactions
  addTransaction(Model.Transaction transaction) async{
    final db = await database;
    List<CartProduct> products = transaction.products;
    Map<String, dynamic> temp;
    for(CartProduct product in products){
      temp = product.toMap();
      temp.putIfAbsent("idtransaction", () => transaction.id);
      await db.insert('TransactionProducts', temp, conflictAlgorithm: ConflictAlgorithm.replace);
    }
    Map<String, dynamic> tempTransaction = transaction.toMap();
    tempTransaction.remove('products');
    await db.insert('Transactions',tempTransaction, conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<List<Model.Transaction>> getAllTransactions() async {
    final db = await database;
    try {
      var res = await db.query("Transactions");
      List<Model.Transaction> transactions =
          res.isNotEmpty ? res.map((c) => Model.Transaction.fromMap2(c)).toList() : [];
      List<CartProduct> products;
      for(Model.Transaction transaction in transactions){
        var res2 = await db.query('TransactionProducts', where: "idtransaction = ?", whereArgs: [transaction.id]);
        products = res2.isNotEmpty ? res2.map((e) => CartProduct.fromMap(e)).toList() :[];
        transaction.products = products;
        print(transaction);
      }
      return transactions;
    } on Exception catch (e) {
      await db.execute("CREATE TABLE Transactions ("
          "_id TEXT PRIMARY KEY,"
          "user_id TEXT,"
          "store_id, INTEGER"
          "payment_type TEXT,"
          "product_count INTEGER,"
          "total INTEGER,"
          "date TEXT"
          ")"
      );
      var res = await db.query("Transactions");
      List<Model.Transaction> transactions =
          res.isNotEmpty ? res.map((c) => Model.Transaction.fromMap2(c)).toList() : [];
      List<CartProduct> products;
      for(Model.Transaction transaction in transactions){
        var res2 = await db.query('TransactionProducts', where: "idtransaction = ?", whereArgs: [transaction.id]);
        products = res2.isNotEmpty ? res2.map((e) => CartProduct.fromMap(e)).toList() :[];
        transaction.products = products;
      }
      return transactions;
    }
  }  

}
