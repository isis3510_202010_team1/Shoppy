import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' show Client;
import 'package:shoppy/models/user_model.dart';
import 'package:shoppy/services/db.dart';
import 'dart:io';

class UsersApiProvider {
  Client client = Client();
  String urlBase = "http://ec2-18-212-16-222.compute-1.amazonaws.com:8083/";
  String localUrl = "http://192.168.0.3:5000/";

  Future<String> login(String email, String passHash) async {
    print("Loggin in or at least trying to");
    Map args = {"email": email, 'pass_hash': passHash};
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(urlBase + 'users/login_user'));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(json.encode(args)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    
    if (response.statusCode == 200) {
      Map jsonReply = json.decode(reply);
      return jsonReply['_id'];
    } else if (response.statusCode == 403) {
      return "Login Failed: Wrong Credentials";
    } else {
      return "Login Failed: Unexpected Error: ${response.statusCode}";
    }
  }

  Future<String> signIn(Map map) async {
    print("Signin in or at least trying to");
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(urlBase + 'users'));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(json.encode(map)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();

    if (response.statusCode == 200) {
      return "OK";
    } else if(response.statusCode == 422) {
      return "422: There's already an account with that E-Mail!";
    } else {
      return "Sign In Failed Unexpected Error: ${response.statusCode}";
    }
  }

  Future<UserModel> getUserInfo(String id) async{
    UserModel user;
    String urlUsers = "http://18.234.130.116:5000/users/";
    try{
      final response = await client.get(urlUsers+ id);
      if(response.statusCode == 200){
        user=  UserModel.fromJson(json.decode(response.body));
        await DBProvider.db.addUser(user);
      }else{
        throw Exception("Error");
      }
    }on Exception catch(e){
      user = await DBProvider.db.getUser(id);
      if(user ==null){
        throw Exception("Not Available" + e.toString());
      }
    }
    return user;
  }

}
