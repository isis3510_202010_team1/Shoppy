import 'dart:convert';
import 'package:http/http.dart' show Client;
import '../models/location_model.dart';
import '../services/repository.dart';

class LocationUpdater {
  Client client = Client();
  String url = "http://ec2-18-212-16-222.compute-1.amazonaws.com:8086/location";
  String urlBase = "http://ec2-18-212-16-222.compute-1.amazonaws.com:8086/";
  final _repository =Repository();

  reportLocation(latitude, longitud) async{
    String userId = await _repository.getUserId();
    Location location = new Location(lat: latitude, lon:longitud, userId: userId);
    try{
      var body = jsonEncode(location.toMap());
      client.post(url,headers:{"Content-Type":"application/json"}, body:body).then((value) => print(value.body));

    }on Exception catch(e){
      print(e);
    }
  }
}
