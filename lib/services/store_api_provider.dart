import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' show Client;
import '../models/store_model.dart';
import '../services/db.dart';

class StoreApiProvider{
  Client client = Client();
  String urlBase = "http://18.234.130.116:5000";
  String urlAll = "http://18.234.130.116:5000/stores";

  Future<List<StoreModel>> fetchStoreList() async{
    List<StoreModel> stores;
    try{
      final response = await client.get(urlAll);
      if(response.statusCode == 200) {
        stores=  StoreModel.listFromJson(json.decode(response.body));
        for(int i = 0; i< stores.length; i++){
          DBProvider.db.addStore(stores[i]);
        }
      } else {
        throw Exception("failed");
      }
    }on Exception catch(e){
      stores = await DBProvider.db.getAllStores();
      if(stores == null){
        throw Exception("Not Available" + e.toString());
      }
    }
    return stores;
  }

  Future<StoreModel> fetchStore(String storeId) async {
    int id = int.parse(storeId);
    StoreModel store;
    try{
      final response = await client.get(urlBase + "stores/" + storeId);
      if(response.statusCode == 200) {
        return StoreModel.fromJson(json.decode(response.body));
      } else {
        throw Exception("Error");
      }
    } on Exception catch(e){
      store = await DBProvider.db.getStore(id);
      if(store == null){
        throw Exception("Not Available" + e.toString());
      }
    }
    return store;
  }
}