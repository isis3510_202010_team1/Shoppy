import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' show Client;
import 'package:shoppy/models/cart_product_model.dart';

class ProductApiProvider {
  Client client = Client();
  String url = "http://ec2-18-212-16-222.compute-1.amazonaws.com:8081/products";
  String urlBase = "http://ec2-18-212-16-222.compute-1.amazonaws.com:8081/";

  Future<List<CartProduct>> fetchStoreProductList(String storeId) async {
    print("API GET: " + urlBase + "stores/" + storeId + "/products");
    try {
      final response =
          await client.get(urlBase + "stores/" + storeId + "/products");
      if (response.statusCode == 200) {
        return CartProduct.listFromJson(json.decode(response.body));
      } else {
        throw Exception(
            "Something went terribly wrong, on the products API, on fetchStoreProductsList, pls be gentle");
      }
    } on Exception catch (e) {
      throw Exception("No Internet Connection" + e.toString());
    }
  }
}
