import 'dart:async';
import 'package:shoppy/models/cart_product_model.dart';
import '../services/db.dart';
class ShoppingListApiProvider{

  Future<List<CartProduct>> getShoppingList() async{
    List<CartProduct> shoppingList;
    try{
      shoppingList = await DBProvider.db.getShoppingList();
    }on Exception catch(e){
      throw Exception("error" + e.toString());
    }
    return shoppingList;
  }

  addProduct(CartProduct product){
    try{
      DBProvider.db.addProductToShoppingList(product);
    } on Exception catch(e){
      throw Exception("Error "+ e.toString());
    }
  }

  deleteProduct(String id){
    try{
      DBProvider.db.deleteProductFromShoppingList(id);
    } on Exception catch(e){
      throw Exception("Error " + e.toString());
    }
  }

  emptyShoppingList(){
    try{
      DBProvider.db.deleteAllProductsFromShoppingList();
    }on Exception catch(e){
      throw Exception("Error" + e.toString());
    }
  }
}