import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shoppy/models/cart_product_model.dart';
import 'package:shoppy/models/store_model.dart';
import 'package:shoppy/models/user_model.dart';
import 'package:shoppy/models/transaction_model.dart';
import 'package:shoppy/services/db.dart';
import 'package:shoppy/services/transactions_api_provider.dart';
import 'package:shoppy/services/shopping_list_api_provider.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'product_api_provider.dart';
import 'usersApiProvider.dart';
import 'store_api_provider.dart';
import 'dart:io';

class Repository {
  static Repository _instance;

  static Repository getInstance() {
    if (_instance == null) {
      _instance = Repository();
    }
    return _instance;
  }

  final productsApiProvider = ProductApiProvider();
  final usersApiProvider = UsersApiProvider();
  final storesApiProvider = StoreApiProvider();
  final transactionsApiProvider = TransactionsApiProvider();
  final shoppingListApiProvider = ShoppingListApiProvider();
  final db = DBProvider.db;

  Future<List<CartProduct>> fetchStoreProducts(String storeId) =>
      productsApiProvider.fetchStoreProductList(storeId);

  Future<List<StoreModel>> fetchAllStores() =>
      storesApiProvider.fetchStoreList();

  Future<StoreModel> fetchStore(String storeId) =>
      storesApiProvider.fetchStore(storeId);
  Future<UserModel> fetchUser(String id) =>
      usersApiProvider.getUserInfo(id);
  Future<bool> isConnected() async{
    bool r = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        // print('connected');
        r = true;
      }
    } on SocketException catch (_) {
      // print('not connected');
      r = false;
    }
    return r;
  }

  Future<String> login(String email, String hashPass) async {
    return usersApiProvider.login(email, hashPass);
  }

  Future<String> signIn(String email, String passHash, String name,
      String phone, String dateOfBirth, String gender) {
    Map map = {
      "email": email,
      "pass_hash": passHash,
      "name": name,
      "phone": phone,
      "date_of_birth": dateOfBirth,
      "gender": gender
    };
    return usersApiProvider.signIn(map);
  }

  Future<String> sendTransaction(Transaction trans) {
    return transactionsApiProvider.sendTransaction(trans);
  }

  void saveUserId(String userId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("user_id", userId);
  }

  Future<String> getUserId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userId = prefs.getString("user_id");
    return userId;
  }

  Future<bool> isLogIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.containsKey("user_id");
  }

  void saveStoreId(String storeId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("store_id", storeId);
  }

  Future<String> getStoreId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey("store_id")) {
      print("deffaulting to store 2");
      return "2";
    }
    String storeId = prefs.getString("store_id");
    return storeId;
  }

  Future<List<Transaction>> fetchUserTransactions(String userId) {
    return transactionsApiProvider.fetchUsersTransactions(userId);
  }

  Future<String> getInvId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey("inv_id")) {
      print("No previous Inventory downloaded");
      return "-1";
    }
    String invId = prefs.getString("inv_id");
    return invId;
  }

  void saveInvId(String invId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("inv_id", invId);
  }

  Future<String> sendStripePayment(String email, PaymentMethod pmt, int total) async {
    String userId = await getUserId();
    return transactionsApiProvider.sendStripePayment(email, pmt, total, userId);
  }

/// Shopping List

  Future<List<CartProduct>> fetchShoppingListProducts() =>
    shoppingListApiProvider.getShoppingList();
  
  addProductToShoppingList(CartProduct product) =>
    shoppingListApiProvider.addProduct(product);
  
  deleteProductFromShoppingList(String id)=>
    shoppingListApiProvider.deleteProduct(id);

  deleteAllInShoppingList()=>
    shoppingListApiProvider.emptyShoppingList();
}
