import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' show Client;
import 'package:shoppy/models/transaction_model.dart';
import 'package:shoppy/services/db.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'dart:io';

class TransactionsApiProvider {
  Client client = Client();
  String urlBase =
      "http://ec2-18-212-16-222.compute-1.amazonaws.com:8085/transactions";
    String urlLocal = 'http://192.168.0.3:5000/transactions';

  Future<List<Transaction>> fetchUsersTransactions(String userId) async {
    List<Transaction> list = [];
    try{
      final response = await client.get(urlBase + '/' + userId);
      if (response.statusCode == 200) {
        var json_rsponse = json.decode(response.body);
        for (int i = 0; i < json_rsponse.length; i++) {
          print(response.body.length);
          list.add(Transaction.fromMap(json_rsponse[i]));
          DBProvider.db.addTransaction(Transaction.fromMap(json_rsponse[i]));
        }
      } else {
        throw Exception('Failed');
      }
    }on Exception catch(e){
      try{
        list = await DBProvider.db.getAllTransactions();
      }on Exception catch(e){
        throw Exception("Error: " + e.toString());
      }
      
    }
    return list;
  }

  Future<String> sendTransaction(Transaction trans) async {
    print("Sending Transaction to DB");
    Map args = trans.toMap();
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(urlBase));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(json.encode(args)));
    sleep(Duration(milliseconds: 500));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();

    if (response.statusCode == 200) {
      return "200";
    } else {
      return "Transaction Failed?: Unexpected Error: ${response.statusCode}";
    }
  }

  Future<String> sendStripePayment(String email, PaymentMethod pmt, int total, String userId) async {
    print("Sending Stripe Payment");
    Map args = {'email':email, 'pmt_id': pmt.id, 'id': userId, 'total':total};
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(urlBase + '/make_payment'));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(json.encode(args)));
    sleep(Duration(milliseconds: 500));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    print(response.statusCode);
    if (response.statusCode == 200) {
      return "200";
    } else {
      return "Transaction Failed?: Unexpected Error: ${response.statusCode}";
    }
  }

  
}
