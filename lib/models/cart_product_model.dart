import 'dart:convert';

CartProduct cartProductFromJson(String str){
  final jsonData = json.decode(str);
  return CartProduct.fromMap(jsonData);
}

String cartProductToJson(CartProduct data){
  final dyn  = data.toMap();
  return json.encode(dyn);
}

class CartProduct{
  String id;
  String name;
  int price;
  String sku;
  int storeId;
  String imgUrl;
  String description;
  int amount;

  static List<CartProduct> listFromJson(List<dynamic> json){
    List<CartProduct> temp = [];
    CartProduct product;
    for(int i = 0; i < json.length; i++){
      product = CartProduct.fromMap(json[i]);
      temp.add(product);
    }
    return temp;
  }

  CartProduct({
    this.id,
    this.name,
    this.price,
    this.sku,
    this.storeId,
    this.imgUrl,
    this.description,
    this.amount,
});

  factory CartProduct.fromMap(Map<String, dynamic> json) => new CartProduct(
    id : json['_id'],
    name : json['name'],
    price : json['price'],
    sku : json['sku'],
    storeId: json['store_id'],
    imgUrl: json['img_url'],
    description: json['description'],
    amount : 1,
  );

  Map<String, dynamic> toMap() =>{
    '_id' : id,
    'name' : name,
    'price': price,
    'sku' : sku,
    'store_id' : storeId,
    'img_url' : imgUrl,
    'description' : description,
    'amount' : amount,
  };
}