
import 'package:shoppy/models/cart_product_model.dart';

class Transaction {
  String id;
  String user_id;
  String store_id;
  List<CartProduct> products;
  String payment_type;
  int product_count;
  int total;
  String date;

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      '_id': id,
      'user_id': user_id,
      'store_id': store_id,
      'payment_type': payment_type,
      'product_count': product_count,
      'total': total,
      'date': date
    };
    List<Map> products_map = [];
    for (int i = 0; i < products.length; i++) {
      products_map.add(products[i].toMap());
    }
    map.putIfAbsent('products', () => products_map);
    return map;
  }

  Transaction(
      {this.id,
      this.user_id,
      this.store_id,
      this.products,
      this.payment_type,
      this.product_count,
      this.total,
      this.date});

  factory Transaction.fromMap(Map<String, dynamic> map) {
    List<CartProduct> products = [];
    List list = map['products'];
    CartProduct c;
    for(int i = 0; i < list.length; i++) {
      c = CartProduct.fromMap(list[i]);
      products.add(c);
    }
    return new Transaction(
      id: map['_id'],
      user_id: map['user_id'],
      store_id: map['store_id'].toString(),
      products: products,
      payment_type: map['payment_type'],
      product_count: map['product_count'],
      total: map['total'],
      date: map['date'],
    );
  }

  factory Transaction.fromMap2(Map<String, dynamic> map) {
    return new Transaction(
      id: map['_id'],
      user_id: map['user_id'],
      store_id: map['store_id'].toString(),
      payment_type: map['payment_type'],
      product_count: map['product_count'],
      total: map['total'],
      date: map['date'],
    );
  }
}
