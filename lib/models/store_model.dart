import 'dart:convert';

StoreModel storeFromJson(String str){
  final jsonData = json.decode(str);
  return StoreModel.fromMap(jsonData);
}

String storeToJson(StoreModel data){
  final dyn  = data.toMap();
  return json.encode(dyn);
}

class StoreModel{
  int id;
  String name;
  String address;
  int phone;
  String city;
  double loclon;
  double loclat;
  String logoimg;
  int currentUsers;

  static List<StoreModel> listFromJson(List<dynamic> json){
    List<StoreModel> temp = [];
    StoreModel store;
    for(int i = 0; i < json.length; i++){
      store = StoreModel.fromMap(json[i]);
      temp.add(store);
    }
    return temp;
  }

  static StoreModel fromJson(List<dynamic> json){
    StoreModel store = StoreModel.fromMap(json[0]);
    return store;
  }

  StoreModel({
    this.id,
    this.name,
    this.address,
    this.phone,
    this.city,
    this.loclon,
    this.loclat,
    this.logoimg,
    this.currentUsers,
  });

  factory StoreModel.fromMap(Map<String, dynamic> json) => new StoreModel(
      id : json['id'],
      name : json['name'],
      address: json['address'],
      phone: json['phone'],
      city: json['city'],
      loclon: json['loc_lon'],
      loclat: json['loc_lat'],
      logoimg: json['logo_img'],
      currentUsers: json['current_users'] == null? 0:json['current_users'],
  );

  Map<String, dynamic> toMap() =>{
    'id' : id,
    'name' : name,
    'phone' : phone,
    'city' : city,
    'address' : address,
    'loclon' : loclon,
    'loclat' : loclat,
    'logoimg' : logoimg,
    'currentusers':currentUsers,
  };
}