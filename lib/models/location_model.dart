class Location {
  
  double lon;
  double lat;
  String userId;

  Location({
    this.lat,
    this.lon,
    this.userId,
  });

  Map<String, dynamic> toMap() =>{
    'lat' : lat,
    'lon' : lon,
    'user_id': userId,
  };

}
