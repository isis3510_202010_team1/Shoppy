import 'dart:convert';

UserModel storeFromJson(String str){
  final jsonData = json.decode(str);
  return UserModel.fromMap(jsonData);
}

String storeToJson(UserModel data){
  final dyn  = data.toMap();
  return json.encode(dyn);
}

class UserModel{
  String id;
  String name;
  String email;
  String gender;
  String date_of_birth;
  String phone;


  static UserModel fromJson(List<dynamic> json){
    UserModel store = UserModel.fromMap(json[0]);
    return store;
  }

  UserModel({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.gender,
    this.date_of_birth,
  });

  factory UserModel.fromMap(Map<String, dynamic> json) => new UserModel(
    id : json['_id'],
    name : json['name'],
    email : json['email'],
    gender : json['gender'],
    date_of_birth : json['date_of_birth'],
    phone : json['phone'],
  );

  Map<String, dynamic> toMap() =>{
    '_id' : id,
    'name' : name,
    'phone' : phone,
    'email' : email,
    'gender' : gender,
    'date_of_birth' : date_of_birth,
  };
}