import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'routes.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences.getInstance().then((prefs) {
    runApp(Shoppy(prefs: prefs));
  });
}

class Shoppy extends StatelessWidget {
  final SharedPreferences prefs;

  Shoppy({this.prefs});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Shoppy',
      theme: ThemeData(
        // brightness: Brightness.dark,
       primaryColor: Color(0xFF1A2930),
        accentColor:  Color(0xFFF7CE3E),
        

        // Define the default font family.
        // fontFamily: 'Georgia',

        // Define the default TextTheme. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
      ),
      initialRoute: _defInitialRoute(),
      routes: routes,
    );
  }

  String _defInitialRoute() {
    if(prefs.containsKey("user_id")) {
      print("Starting with user logged in");
      return "/storesMap";
    } else {
      print("Starting without user login, login view loaded");
      return "/login";
    }
  }
}
