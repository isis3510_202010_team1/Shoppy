import 'dart:async';
import '../models/transaction_model.dart';
import '../services/repository.dart';

class TransactionBloc {
  final _transactionController = StreamController<List<Transaction>>.broadcast();
  final _repository =Repository();
  get transactions => _transactionController.stream;
  String userId;
  dispose(){
    _transactionController.close();
  }

  getTransactions(String id) async{
    List<Transaction> transaction =  await _repository.fetchUserTransactions(id);
    _transactionController.sink.add(transaction);
  }


  initialize() async{
    userId = await _repository.getUserId();
    getTransactions(userId);
  }

}