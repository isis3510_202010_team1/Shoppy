import 'dart:async';

import 'package:shoppy/components/Toast.dart';
import 'package:shoppy/models/store_model.dart';
import 'package:shoppy/services/repository.dart';

import '../models/cart_product_model.dart';
import '../services/db.dart';

class ShoppingBloc {
  final _shoppingControler = StreamController<List<CartProduct>>.broadcast();
  final repository = Repository();
  get storeProducts => _shoppingControler.stream;

  dispose() {
    _shoppingControler.close();
  }

  String storeId;
  bool connected = true;
  String emptyText = "None";

  Future<String> getEmptyText() async {
    List<CartProduct> localProductList =
        await DBProvider.db.getAllStoreProducts();
      print("Getting Empty Text: list size: " + localProductList.length.toString());
    if(localProductList.length == 0) {
      return "The store's products were not downloaded because there is no internet, please try again later";
    } else {
      return "The shopping cart is empty\nScan some items!";
    }
  }

  fetchStoreProducts() async {
    print("StoreInvBloc: getting products: " + storeId);
    List<CartProduct> localProductList =
        await DBProvider.db.getAllStoreProducts();
    String lastInv = await repository.getInvId();
    if (lastInv == storeId && localProductList.length > 0) {
      print("Productos ya fueron descargados: " + lastInv);
    } else {
      if (!await isConnected()) {
        await DBProvider.db.deleteAllVirtualCartProducts();
        await DBProvider.db.deleteAllStoreProducts();
        getVirtualCartProducts();
        return;
      }
      await DBProvider.db.deleteAllVirtualCartProducts();
      await DBProvider.db.deleteAllStoreProducts();
      print("Descargando Productos porque lastInv: " + lastInv + ":" + storeId);
      repository.saveInvId(storeId);
      await repository.fetchStoreProducts(storeId).then((value) {
        List<CartProduct> remoteList = value;
        for (int i = 0; i < remoteList.length; i++) {
          DBProvider.db.addStoreProduct(remoteList[i]);
        }
      }).catchError((exception) {});
    }
    getVirtualCartProducts();
  }

  Future<List<CartProduct>> getAllStoreProducts() async {
    return DBProvider.db.getAllStoreProducts();
  }

  Future<CartProduct> getStoreProductId(String id) async {
    return DBProvider.db.getStoreProduct(id);
  }

  initialize() async {
    storeId = await repository.getStoreId();
    print("StoreInvBLOC: INIT: StoresProductsBloc: Store: " + storeId);
    getVirtualCartProducts();
    fetchStoreProducts();
  }

  deleteStoreInventory() {
    print("StoreInvBLOC: Clearing current store inventory");
    DBProvider.db.deleteAllStoreProducts();
    DBProvider.db.deleteAllVirtualCartProducts();
    getVirtualCartProducts();
  }

  addProductToStoreProducts(CartProduct product) {
    DBProvider.db.addStoreProduct(product);
    getVirtualCartProducts();
  }

  deleteAllTables() {
    print("DELETING ALL TABLES AAAAAAAAAA");
    DBProvider.db.delteAllTables();
    getVirtualCartProducts();
  }

  getVirtualCartProducts() async {
    print("StoreInvBloc: getting products");
    List<CartProduct> localProductList =
        await DBProvider.db.getAllVirtualCartProducts();
    _shoppingControler.sink.add(localProductList);
  }

  addProductToVirtualCart(CartProduct product) {
    product.amount = 1;
    DBProvider.db.addVirtualCartProduct(product);
    getVirtualCartProducts();
  }

  Future<bool> isConnected() async {
    connected = await repository.isConnected();
    return connected;
  }

  deleteProductFromVirtualCart(String id) {
    DBProvider.db.deleteVirtualCartProduct(id);
    getVirtualCartProducts();
  }

  Future<int> getTotalPrice() async {
    print("Getting total price");
    List<CartProduct> products =
        await DBProvider.db.getAllVirtualCartProducts();
    int total = 0;
    for (int i = 0; i < products.length; i++) {
      total += products[i].price;
    }
    print("Returned total price");
    return total;
  }

  Future<String> getStoreId() async {
    return repository.getStoreId();
  }

  Future<String> getStoreName() async {
    StoreModel store = await DBProvider.db.getStore(int.parse(await getStoreId()));
    return store.name;
  }
}
