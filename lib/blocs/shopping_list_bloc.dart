import 'dart:async';

import 'package:shoppy/models/cart_product_model.dart';
import '../services/repository.dart';

class ShoppingListBloc {
  final _shoppingListController = StreamController<List<CartProduct>>.broadcast();
  final Repository _repository = new Repository();
  get shoppingList => _shoppingListController.stream;

  dispose(){
    _shoppingListController.close();
  }

  getShoppingList() async{
    List<CartProduct> shoppingList =  await _repository.fetchShoppingListProducts();
    _shoppingListController.sink.add(shoppingList);
  }

  ShoppingListBloc(){
    getShoppingList();
  }

}