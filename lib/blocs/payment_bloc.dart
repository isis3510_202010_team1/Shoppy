import 'dart:async';
import 'package:shoppy/models/cart_product_model.dart';
import 'package:shoppy/models/transaction_model.dart';
import 'package:shoppy/services/db.dart';
import 'package:stripe_payment/stripe_payment.dart';

import '../services/repository.dart';

class PaymentBloc {
  Repository repo = Repository.getInstance();

  final _virtualCartControler = StreamController<List<CartProduct>>.broadcast();
  final _transactionInfoController =
      StreamController<Map<String, int>>.broadcast();

  get virtualCartProducts => _virtualCartControler.stream;
  get transactionInfo => _transactionInfoController.stream;

  dispose() {
    _virtualCartControler.close();
    _transactionInfoController.close();
  }

  streamVirtualCartProducts() async {
    print("PaymentBloc: Fetching Virtual Cart Products");
    List<CartProduct> productList =
        await DBProvider.db.getAllVirtualCartProducts();
    _virtualCartControler.sink.add(productList);
  }

  deleteVirtualCart() async {
    DBProvider.db.deleteAllVirtualCartProducts();
  }

  streamTransactionInfo() async {
    print("PaymentBloc: Sending TransactionInfo");
    Map<String, int> map;
    List<CartProduct> productList =
        await DBProvider.db.getAllVirtualCartProducts();
    int total = 0;
    for (int i = 0; i < productList.length; i++) {
      total += productList[i].amount * productList[i].price;
    }
    double iva = 1.19;
    int subtotal = (total / iva).round();
    int ivaPayed = total - subtotal;
    map = {'total': total, 'subtotal': subtotal, 'iva': ivaPayed};
    _transactionInfoController.sink.add(map);
  }

  Future<String> sendTransactionCC() async {
    List<CartProduct> productList =
        await DBProvider.db.getAllVirtualCartProducts();
    int total = 0;
    for (int i = 0; i < productList.length; i++) {
      total += productList[i].amount * productList[i].price;
    }
    Transaction trans = Transaction(
        products: productList,
        date: "",
        id: "",
        payment_type: "credit_card",
        store_id: await repo.getStoreId(),
        product_count: 0,
        total: total,
        user_id: await repo.getUserId());
    String r = await repo.sendTransaction(trans);
    return r;
  }

  Future<bool> isConnected() async {
    bool connected = await repo.isConnected();
    return connected;
  }

  Future<String> makePayment(PaymentMethod pmt) async {
    List<CartProduct> productList =
        await DBProvider.db.getAllVirtualCartProducts();
    int total = 0;
    for (int i = 0; i < productList.length; i++) {
      total += productList[i].amount * productList[i].price;
    }
    return repo.sendStripePayment("test@test.com", pmt, total);
  }
}
