import 'dart:async';

import 'package:shoppy/models/store_model.dart';
import '../services/repository.dart';

class StoreBloc {
  final _storesController = StreamController<List<StoreModel>>.broadcast();
  final _repository =Repository();
  get stores => _storesController.stream;
  Stream<List<StoreModel>> get allStores => _storesController.stream;

  dispose(){
    _storesController.close();
  }

  getAllStores() async{
    List<StoreModel> stores =  await _repository.fetchAllStores();
    _storesController.sink.add(stores);
  }

  StoreBloc(){
    getAllStores();
  }

}