import 'dart:async';

import 'package:shoppy/models/user_model.dart';
import '../services/repository.dart';

class UserBloc {
  final _userController = StreamController<UserModel>.broadcast();
  final _repository =Repository();
  get user => _userController.stream;
  String userId;
  dispose(){
    _userController.close();
  }

  getUser(String id) async{
    UserModel user =  await _repository.fetchUser(id);
    _userController.sink.add(user);
  }


  initialize() async{
    userId = await _repository.getUserId();
    getUser(userId);
  }

}