import 'dart:async';

import 'package:shoppy/models/cart_product_model.dart';

import '../services/repository.dart';

class StoreProductsBloc {
  final _storeProductsController = StreamController<List<CartProduct>>.broadcast();
  final _repository =Repository();
  get storeProducts => _storeProductsController.stream;
  dispose(){
    _storeProductsController.close();
  }
  getStoreProducts(String id) async{
    List<CartProduct> storeProducts =  await _repository.fetchStoreProducts(id);
    _storeProductsController.sink.add(storeProducts);
  }

}