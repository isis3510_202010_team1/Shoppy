import 'fab_bottom_bar_item.dart';
import 'package:flutter/material.dart';

class NavBar extends StatefulWidget {
  NavBar({
    this.index,
  });

  final int index;

  @override
  State<StatefulWidget> createState() => NavBarState();
}

class NavBarState extends State<NavBar> {
  @override
  void initState() {
    super.initState();
    selected = widget.index;
    _getRoutes();
  }

  static int selected;
  List<String> routes = new List<String>();

  _getRoutes() {
    routes.add('/storesMap');
    routes.add('/stores');
    routes.add('/shoppingList');
    routes.add('/profile');
  }

  void _selectedFab(int index) {
    selected = widget.index;
    if (index != selected) {
      if (index == 0) {
        Navigator.of(context).pushNamedAndRemoveUntil(
        routes[index], (Route<dynamic> route) => false);
      }else{
        if (selected == 0) {
          Navigator.of(context).pushNamedAndRemoveUntil(
          routes[index], (Route<dynamic> route) => false);  
        } else {
          Navigator.of(context).pushNamedAndRemoveUntil(
          routes[index], (Route<dynamic> route) => false);
        }
      }

    }
  }

  @override
  Widget build(BuildContext context) {
    return FABBottomAppBar(
      centerItemText: 'Shop',
      color: Color(0xFFC5C1C0),
      selectedColor: Color(0xFF1A2930),
      notchedShape: CircularNotchedRectangle(),
      onTabSelected: _selectedFab,
      firstIndex: widget.index,
      items: [
        FABBottomAppBarItem(iconData: Icons.store, text: 'Stores Map'),
        FABBottomAppBarItem(iconData: Icons.search, text: 'Search'),
        FABBottomAppBarItem(iconData: Icons.list, text: 'Shopping List'),
        FABBottomAppBarItem(iconData: Icons.person, text: 'Profile'),
      ],
    );
  }
}
