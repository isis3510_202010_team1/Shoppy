
import 'package:flutter/material.dart';

class FloatingNav extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => FloatingNavState();
}

class FloatingNavState extends State<FloatingNav> {
  @override
  void initState() {
    super.initState();
  }

  _goShop(){
    // Navigator.pushNamed(context, "/scanner");
    Navigator.of(context).pushNamedAndRemoveUntil(
          '/scanner', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: _goShop,
      tooltip: 'Shop',
      child: Icon(Icons.shopping_cart, color: Theme.of(context).primaryColor,size: 35,),
      elevation: 3.0,
      backgroundColor: Theme.of(context).accentColor,
    );
  }

}