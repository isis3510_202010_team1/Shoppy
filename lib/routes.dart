import 'package:flutter/widgets.dart';
import 'package:shoppy/ui/stripe_thing.dart';
import 'package:shoppy/services/repository.dart';
import 'package:shoppy/ui/product_list.dart';
import 'package:shoppy/ui/profile.dart';
import 'package:shoppy/ui/transactions.dart';
import 'ui/ShoppingView.dart';
import 'ui/stores_map.dart';
import 'ui/login.dart';
import 'ui/createAccount.dart';
import 'ui/payment.dart';
import 'ui/store_list.dart';
import 'ui/shopping_list.dart';

final Repository _repository = new Repository();

final Map<String,WidgetBuilder> routes = <String, WidgetBuilder>{
  "/stores": (BuildContext context) => StoreList(),
  "/storesMap" : (BuildContext context) => StoresMap(),
  "/scanner" : (BuildContext context) => ShoppingView(),
  "/login": (BuildContext context) => Login(),
  "/createAccount": (BuildContext context) => CreateAccount(),
  "/payment": (BuildContext context) => Payment(),
  "/stripe": (BuildContext context) => StripeThing(),
  "/profile": (BuildContext context) => Profile(),
  "/transactions" : (BuildContext context) => Transactions(),
  "/storeProducts" : (BuildContext context) => ProductList(storeId: _repository,),
  "/shoppingList" : (BuildContext context) => ShoppingList(),
};