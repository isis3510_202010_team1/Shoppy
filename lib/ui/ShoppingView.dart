import 'dart:async';
import 'dart:io';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shoppy/blocs/shopping_bloc.dart';
import 'package:shoppy/components/Toast.dart';
import 'package:shoppy/components/floating_navigator.dart';
import 'package:shoppy/components/nav_bar.dart';
import 'package:shoppy/models/cart_product_model.dart';

class ShoppingView extends StatefulWidget {
  ShoppingView();

  @override
  State<StatefulWidget> createState() {
    return ShoppingViewState();
  }
}

class ShoppingViewState extends State<ShoppingView> {
  @override
  void initState() {
    shoppingBloc.initialize();
    updateTotal();
    canFetch();
    sleep(Duration(milliseconds: 100));
    super.initState();
  }
  canFetch() async {
    List<CartProduct> products = await shoppingBloc.getAllStoreProducts();
    if (products.length == 0) {
      setState(() {
        couldFetch = true;
      });
    } else {
      setState(() {
        couldFetch = true;
      });
    }
  }

  final shoppingBloc = ShoppingBloc();
  String barcode = "";
  bool couldFetch = true;

  final totalController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    print("Creating Scanner...");
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        actions: <Widget>[
          IconButton(
            onPressed: scan,
            icon: Icon(Icons.center_focus_weak,
                color: Theme.of(context).accentColor),
            iconSize: 40,
          ),
          IconButton(
            onPressed: pay,
            icon: Icon(Icons.payment, color: Theme.of(context).accentColor),
            iconSize: 40,
          ),
        ],
        title: FutureBuilder(
            future: shoppingBloc.getStoreName(),
            builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
              if (snapshot.hasData) {
                return Text("Store: " + snapshot.data,
                    style: TextStyle(color: Theme.of(context).accentColor));
              } else {
                return Text("Store: ");
              }
            }),
      ),
      body: couldFetch
          ? Stack(
              children: <Widget>[
                StreamBuilder(
                    stream: shoppingBloc.storeProducts,
                    builder: (BuildContext context,
                        AsyncSnapshot<List<CartProduct>> snapshot) {
                      if (!snapshot.hasData || snapshot.data.length == 0) {
                        return Center(
                          child: FutureBuilder(
                            future: shoppingBloc.getEmptyText(),
                            builder: (BuildContext context,
                                AsyncSnapshot<String> snapshot) {
                              return Text(
                                "${snapshot.data}",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 20),
                              );
                            },
                          ),
                        );
                      } else {
                        return Container(
                          child: listWidget(snapshot),
                          height: MediaQuery.of(context).size.height * 0.8,
                        );
                      }
                    }),
                // Positioned(
                //   bottom: 20,
                //   // left: 135,
                //   child: new Align(
                //     alignment: FractionalOffset.bottomCenter,
                //     child: IconButton(
                //       onPressed: scan,
                //       icon: Icon(Icons.center_focus_weak,
                //           color: Theme.of(context).primaryColor),
                //       iconSize: 80,
                //     ),
                //   ),
                // )
                // Total Floating Text
                Positioned(
                    bottom: 20,
                    child: Container(
                      margin: EdgeInsets.fromLTRB(20, 0, 0, 10),
                      child: SizedBox(
                          height: 50,
                          width: 200,
                          child: TextField(
                            style: TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold),
                            readOnly: true,
                            controller: totalController,
                            decoration:
                                InputDecoration(border: InputBorder.none),
                          )),
                    ))
              ],
            )
          : Center(
              child: Stack(
                children: <Widget>[
                  Center(
                    child: Text(
                        'We\'re sorry there\'s no internet connection. \n There\'s nothing to Scan. \n Come back soon.',
                        style: TextStyle(
                            fontSize: 30,
                            color: Colors.black54,
                            fontFamily: "Montserrat"),
                        textAlign: TextAlign.center),
                  ),
                  Positioned(
                    bottom: 100,
                    left: 135,
                    child: new Align(
                      alignment: FractionalOffset.bottomCenter,
                      child: FlatButton(
                        onPressed: () =>
                            Navigator.pushNamed(context, "/storesMap"),
                        child: Text(
                          "GoBack",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Colors.blue,
                      ),
                    ),
                  )
                ],
              ),
            ),
      bottomNavigationBar: NavBar(
        index: -1,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingNav(),
    );
  }

  Widget listWidget(AsyncSnapshot<List<CartProduct>> snapshot) {
    print("Creating Shopping view List: " + snapshot.data.length.toString());
    return ListView.builder(
      itemCount: snapshot.data.length,
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            // color: Colors.white70,
            color: Theme.of(context).primaryColor,
            margin: EdgeInsets.symmetric(horizontal: 30, vertical: 0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                ListTile(
                  title: Text(
                    '${snapshot.data[index].name}',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  subtitle: Text(
                    '\$${snapshot.data[index].price}',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.normal,
                        color: Colors.white),
                  ),
                ),
                ButtonBar(
                  alignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                      color: Theme.of(context).accentColor,
                      onPressed: () => removeFromCart(snapshot.data[index].id),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Icon(
                            Icons.remove_shopping_cart,
                            color: Theme.of(context).primaryColor,
                          ),
                          Text(
                            'Remove',
                            style: TextStyle(
                                color: Theme.of(context).primaryColor),
                          ),
                        ],
                      ),
                    ),
                    FlatButton(
                      color: Theme.of(context).accentColor,
                      onPressed: () => _detailPage(snapshot.data[index], index),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Icon(Icons.info,
                              color: Theme.of(context).primaryColor),
                          Text(
                            'Details',
                            style: TextStyle(color: Colors.black),
                          )
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Future scan() async {
    try {
      String barcode = (await BarcodeScanner.scan()).rawContent;
      ScanResult a; 
      setState(() => this.barcode = barcode);
      addScannedProduct(barcode);
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
        print("Unknown Error");
      }
    } on FormatException {
      setState(() => this.barcode =
          'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
      setState(() => this.barcode = 'Unknown error: $e');
    }
  }

  updateTotal() async {
    print("UPDATING TOTAL");
    int total = await shoppingBloc.getTotalPrice();
    setState(() {
      totalController.text = "Total: \$" + total.toString();
    });
  }

  addScannedProduct(String scannedId) async {
    print("SCANNING PRODUCT");
    CartProduct p = await shoppingBloc.getStoreProductId(scannedId);
    if (p != null) {
      shoppingBloc.addProductToVirtualCart(p);
    } else {
      print("Unknown Code: " + scannedId);
      Toaster.showLongToast("Unknown product code: " + scannedId);
    }
    updateTotal();
  }

  removeFromCart(String id) {
    print("Removing Product: " + id);
    shoppingBloc.deleteProductFromVirtualCart(id);
    updateTotal();
  }

  void pay() async {
    print("PAAAAAAAAAYYYY");
    if (await shoppingBloc.getTotalPrice() == 0) {
      Toaster.showLongToast("There are no products in the cart!");
      return;
    } else if (!await shoppingBloc.isConnected()) {
      Toaster.showLongToast(
          "There's no Internet Connection, please try again when connected");
      return;
    }
    print("Pressed PAY NOW, lets get the money");
    Navigator.pushNamed(context, '/payment');
  }

  _detailPage(CartProduct data, int index) {}
}
