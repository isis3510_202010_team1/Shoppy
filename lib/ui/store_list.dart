import 'package:flutter/material.dart';
import 'package:shoppy/components/floating_navigator.dart';
import 'package:shoppy/components/nav_bar.dart';
import '../models/store_model.dart';
import '../blocs/stores_bloc.dart';
import 'package:shoppy/ui/product_list.dart';

class StoreList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => StoreListState();
}

class StoreListState extends State<StoreList> {
  final StoreBloc storesBloc = new StoreBloc();

  @override
  void initState() {
    storesBloc.getAllStores();
    super.initState();
  }

  @override
  void dispose() {
    storesBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Stores',
          style: TextStyle(
            color: Color(0xFFF7CE3E),
            fontSize: 27,
          ),
        ),
        centerTitle: true,
        backgroundColor: Color(0xFF1A2930),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 5, bottom: 30),
        child: Stack(
          children: [
            buildStores(),
          ],
        ),
      ),
      bottomNavigationBar: NavBar(
        index: 1,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingNav(),
    );
  }

  Widget buildStores() {
    return StreamBuilder(
      stream: storesBloc.stores,
      builder: (context, AsyncSnapshot<List<StoreModel>> snapshot) {
        if (snapshot.hasData) {
          return new ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Card(
                    color: Color(0xFF1A2930),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    elevation: 5.0,
                    child: Column(
                      children: [
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ListTile(
                              leading: snapshot.data[index].currentUsers != null && snapshot.data[index].currentUsers >=40 ?Icon(Icons.report_problem, color: Color(0xFFF32013), size: 50,) : Icon(Icons.store, color: Color(0xFFF7CE3E), size: 50,),
                              title: Text(snapshot.data[index].name, style: TextStyle(color: Color(0xFFFFFFFF),fontSize: 25),),
                              subtitle: Text("${snapshot.data[index].city} : ${snapshot.data[index].address}",style: TextStyle(color: Color(0xFFFFFFFF))),
                            ),
                            snapshot.data[index].currentUsers != null && snapshot.data[index].currentUsers >= 40 ?
                            ListTile(
                              leading: Icon(Icons.people, color: Color(0xFFF32013), size: 50,),
                              title: Text("${snapshot.data[index].currentUsers}", style: TextStyle(color: Color(0xFFFFFFFF), fontSize: 25),),
                              subtitle: Text("Amount of people in store",style: TextStyle(color: Color(0xFFC5C1C0), fontSize: 15)),
                            ):ListTile(
                              leading: Icon(Icons.person, color: Color(0xFFF7CE3E), size: 50,),
                              title: Text("${snapshot.data[index].currentUsers}", style: TextStyle(color: Color(0xFFFFFFFF), fontSize: 25),),
                              subtitle: Text("Amount of people in store",style: TextStyle(color: Color(0xFFC5C1C0), fontSize: 15)),
                            ),
                            FlatButton(
                              color: Color(0xFFF7CE3E),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              onPressed: () => _goToStore(snapshot.data[index]),
                              padding: EdgeInsets.symmetric(vertical: 10,horizontal: 30),
                              child: Text('Check Products',style: TextStyle(color: Color(0xFF1A2930),fontSize: 18)),
                            ),
                            Padding(
                              padding: EdgeInsets.only(bottom: 15),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              });
        } else if (!snapshot.hasData) {
          return Center(child: Column(
            children: [
              Padding(padding: EdgeInsets.only(top: 150),),
              Image(image: AssetImage('assets/images/empty.png'),height: 100,alignment: Alignment.center,),
              Text("This is Empty sorry", style: TextStyle(color: Color(0xFF1A2930), fontSize: 30), textAlign: TextAlign.center,),
            ],
          ),
          );
        } else if (snapshot.hasError) {
          return Text(snapshot.error.toString());
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }

  _goToStore(StoreModel store){
  Navigator.push(context, MaterialPageRoute(builder: (context){
    return ProductList(
      storeId: store.id,
      storeName: store.name,
      call: 1,
    );
  })
  );
}
}