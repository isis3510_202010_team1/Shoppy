import 'package:flutter/material.dart';
import 'package:shoppy/services/repository.dart';
import 'package:shoppy/models/cart_product_model.dart';
import 'package:cached_network_image/cached_network_image.dart';


class ProductDetail extends StatefulWidget {
  final id;
  final name;
  final price;
  final sku;
  final storeId;
  final imgUrl;
  final description;
  final amount;
  final added;

  ProductDetail(
      {this.id,
      this.name,
      this.price,
      this.sku,
      this.storeId,
      this.imgUrl,
      this.description,
      this.amount,
      this.added});

  @override
  State<StatefulWidget> createState() {
    return ProductDetailState(
        id: id,
        name: name,
        price: price,
        sku: sku,
        storeId: storeId,
        imgUrl: imgUrl,
        description: description,
        amount: amount,
        added: added);
  }
}

class ProductDetailState extends State<ProductDetail> {
  final id;
  final name;
  final price;
  final sku;
  final storeId;
  final imgUrl;
  final description;
  final amount;
  final added;
  final Repository _repository = new Repository();
  final String _url = "http://ec2-18-212-16-222.compute-1.amazonaws.com:8082/images/";

  bool _isAdded;
  bool _changed = false;

  ProductDetailState(
      {this.id,
      this.name,
      this.price,
      this.sku,
      this.storeId,
      this.imgUrl,
      this.description,
      this.amount,
      this.added});


  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    print(id);
    return Scaffold(
        appBar: AppBar(
          title: Text(
            name,
            style: TextStyle(
              color: Color(0xFFF7CE3E),
              fontSize: 20,
            ),
          ),
          centerTitle: true,
          backgroundColor: Color(0xFF1A2930),
        ),
        body: Container(
          child: ListView(
            children: [
              Stack(
                children: <Widget>[
                  detailedSection(),
                  Positioned(
                    width: 90,
                    height: 90,
                    top: 10,
                    left: 145,
                    child: Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xFFFFFFFF),
                          image: new DecorationImage(
                              image: CachedNetworkImageProvider("$_url$imgUrl",
                              ),
                              fit: BoxFit.fill)),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        );
  }

  Widget detailedSection(){
    print(_changed);
    if(_changed == false){
      _initialize();
    }
    return Container(
      padding: EdgeInsets.fromLTRB(10, 45, 10, 0),
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 40),
            ),
            ListTile(
              title: Text(
                "\$$price",
                style: TextStyle(
                    color: Color(
                      0xFFFFFFFF,
                    ),
                    fontSize: 20),
                textAlign: TextAlign.center,
              ),
              subtitle: Text(
                "Price",
                style: TextStyle(color: Color(0xFFC5C1C0)),
                textAlign: TextAlign.center,
              ),
            ),
            ListTile(
              title: Text(
                "$name",
                style: TextStyle(
                  color: Color(
                    0xFFFFFFFF,
                  ),
                  fontSize: 20,
                ),
              textAlign: TextAlign.center,
              ),
              subtitle: Text(
                "Name",
                style: TextStyle(color: Color(0xFFC5C1C0)),
                textAlign: TextAlign.center,
              ),
            ),
            ListTile(
              title: Text(
                 description,
                style: TextStyle(
                  color: Color(
                    0xFFFFFFFF,
                  ),
                  fontSize: 18,
                ),
                textAlign: TextAlign.start,
              ),
              subtitle: Text(
                "Description",
                style: TextStyle(color: Color(0xFFC5C1C0)),
              ),
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceEvenly,
              children: [
                FlatButton(
                  color: Color(0xFFF7CE3E),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  onPressed:_add,
                  padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Icon(_isAdded ? Icons.playlist_add_check : Icons.playlist_add, color: Color(0xFF1A2930),size: 27,),
                      Padding(
                              padding: EdgeInsets.symmetric(horizontal: 5),
                            ),
                            Text(_isAdded ? "Remove" : "Add" , style :TextStyle(color: Color(0xFF1A2930),fontSize: 18),),
                          ],
                        ) ,
                      ),
                      FlatButton(
                        color: Color(0xFFF7CE3E),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        onPressed:_detailPage,
                        padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Icon( Icons.arrow_back , color: Color(0xFF1A2930),size: 27, ) ,
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 5),
                            ),
                            Text("Go Back", style :TextStyle(color: Color(0xFF1A2930),fontSize: 18))
                    ],
                  ) ,
                ),
                            
              ],
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10),
            )
          ],
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        elevation: 5.0,
        color: Color(0xFF1A2930),
      ),
    );
  }

  _add(){
    bool addTemp = _isAdded;
    CartProduct product = new CartProduct(id: id,amount: amount,description: description,imgUrl: imgUrl,name: name,price: price, sku: sku,storeId: storeId);
    if(addTemp == false){
      _repository.addProductToShoppingList(product);
    }else{
      _repository.deleteProductFromShoppingList(product.id);
    }
    addTemp = !addTemp;
    setState(() {
      _isAdded = addTemp;
      _changed = true;
    });
  }

  _detailPage() {
    if(Navigator.canPop(context)){
      Navigator.pop(context);
    }
  }

  _initialize(){
    _isAdded = added;
  }

}
