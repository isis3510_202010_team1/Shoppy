import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shoppy/blocs/transactions_bloc.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:shoppy/components/floating_navigator.dart';
import 'package:shoppy/components/nav_bar.dart';
import 'package:shoppy/models/cart_product_model.dart';
import 'package:shoppy/models/transaction_model.dart';

class Transactions extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => TransactionsState();
}

class TransactionsState extends State<Transactions> {
  final TransactionBloc txBloc = new TransactionBloc();
  final _url = "http://ec2-18-212-16-222.compute-1.amazonaws.com:8082/images";

  @override
  void initState() {
    txBloc.initialize();
    super.initState();
  }

  @override
  void dispose() {
    txBloc.initialize();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Transactions',
          style: TextStyle(
            color: Color(0xFFF7CE3E),
            fontSize: 20,
          ),
        ),
        centerTitle: true,
        backgroundColor: Color(0xFF1A2930),
      ),
      body: Container(
        child: Stack(
          children: <Widget>[
            Container(
              color: Color(0xFFFFFFFF),
            ),
            transactionSection(),
          ],
        ),
      ),
      bottomNavigationBar: NavBar(
        index: 3,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingNav(),
    );
  }

  Widget transactionSection() {
    return StreamBuilder(
      stream: txBloc.transactions,
      builder: (context, AsyncSnapshot<List<Transaction>> snapshot) {
        if (snapshot.hasData) {
          return new ListView.separated(
            separatorBuilder: (context,index) => Divider(color: Color(0xFF1A2930),),
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: ExpansionTile(
                    leading: Icon(
                      Icons.receipt,
                      size: 50,
                      color: Color(0xFF1A2930),
                    ),
                    backgroundColor: Color(0xFFFFFFFF),
                    trailing: Icon(
                        snapshot.data[index].payment_type == 'credit_card'
                            ? Icons.credit_card
                            : Icons.attach_money, color: Color(0xFFF7CE3E),),
                    title: Text(
                      "Total:\$ ${snapshot.data[index].total}",
                      style: TextStyle(
                        fontSize: 20,
                        color: Color(0xFF1A2930),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    subtitle: Text(
                      "Purchased on: ${_getDate(snapshot.data[index].date)}",
                      style: TextStyle(
                          fontSize: 18,
                          color: Color(0xFF1A2930),
                          fontStyle: FontStyle.italic),
                    ),
                    children: <Widget>[
                      new Column(
                        children: _buildContent(snapshot.data[index].products),
                      )
                    ],
                  ),
                );
              });
        } else if (snapshot.hasError) {
          return Text(snapshot.error.toString());
        } else if(!snapshot.hasData){
          return Center(child: Column(
            children: [
              Padding(padding: EdgeInsets.only(top: 100),),
              Image(image: AssetImage('assets/images/empty.png'),height: 100,alignment: Alignment.center,),
              Text("This is Empty, go buy some stuff.", style: TextStyle(color: Color(0xFF1A2930), fontSize: 30), textAlign: TextAlign.center,),
            ],
          ),
          );
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }

  _buildContent(List<CartProduct> products) {
    List<Widget> columnContent = [];

    for (CartProduct product in products)
      columnContent.add(ListTile(
        leading: CachedNetworkImage(
                imageUrl: "$_url/${product.imgUrl}",
                placeholder: (context, url) => CircularProgressIndicator(),
                errorWidget: (context, url, error) => Icon(
                  Icons.local_offer,
                  color: Color(0xFF1A2930),
                ),
                height: 50,
                width: 50,
              ),
        title: Text(
          product.name,
          style: TextStyle(fontSize: 18,color: Color(0xFF1A2930) ),
        ),
        subtitle: Text("\$${product.price}", style: TextStyle(color: Color(0xFF1A2930)),),
      ));

    return columnContent;
  }

  _getDate(String date) {
    DateFormat format = new DateFormat("EEE, dd MMM yyyy hh:mm:ss zzz");
    DateTime newDate = format.parse(date);
    String result = "${newDate.day}/${newDate.month}/${newDate.year}";
    return result;
  }
}
