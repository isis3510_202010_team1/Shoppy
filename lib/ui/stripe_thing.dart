import 'package:flutter/material.dart';
import 'package:shoppy/blocs/payment_bloc.dart';
import 'package:shoppy/components/Toast.dart';
import 'package:stripe_payment/stripe_payment.dart';

class StripeThing extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return StripeThingState();
  }
}

class StripeThingState extends State<StripeThing> {
  @override
  void initState() {
    StripePayment.setOptions(
      StripeOptions(
        publishableKey: "pk_test_1Io5gDHECnsWKVIKECzytxSe00Kqj9z92J",
        merchantId: "Test",
        androidPayMode: 'test',
      ),
    );
    super.initState();
  }

  PaymentBloc bloc = PaymentBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Stripe Payment!"),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text(
              "Press to add a credit card",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 30),
            ),
            IconButton(
              icon: Icon(Icons.credit_card), iconSize: 300, color: Theme.of(context).primaryColor,
              tooltip: "Pay Now",
              onPressed: payNow,
            ),
          ],
        ),
      ),
    );
  }

  void payNow() async {
    if (!await bloc.isConnected()) {
      Toaster.showLongToast(
          "There's no internet connection, please try again when connected...");
      return;
    }
    PaymentMethod pmt = await StripePayment.paymentRequestWithCardForm(
      CardFormPaymentRequest(),
    ).catchError(setError);
    String response = await bloc.makePayment(pmt);
    print(response);
    if (response == "200") {
      String response2 = await bloc.sendTransactionCC();
      await bloc.deleteVirtualCart();
      Navigator.of(context).pushNamedAndRemoveUntil(
          '/transactions', (Route<dynamic> route) => false);
    } else {
      Toaster.showLongToast("Something went really wrong, we're sorry...");
    }
  }

  void setError(dynamic error) {
    Toaster.showLongToast("Please add a valid card!");
  }
}
