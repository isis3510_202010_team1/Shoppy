import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shoppy/blocs/store_products_bloc.dart';
import 'package:shoppy/components/floating_navigator.dart';
import 'package:shoppy/components/nav_bar.dart';
import 'package:shoppy/models/cart_product_model.dart';
import 'product_detail.dart';
import 'package:shoppy/services/repository.dart';

class ProductList extends StatefulWidget {
  final storeId;
  final storeName;
  final call;

  ProductList({this.storeId, this.storeName, this.call});

  @override
  State<StatefulWidget> createState() {
    return ProductListState(storeId: storeId, storeName: storeName, call: call);
  }
}

class ProductListState extends State<ProductList> {
  final storeId;
  final storeName;
  final call;
  final Repository _repository = new Repository();
  List<bool> _added;
  final StoreProductsBloc storeProductsBloc = new StoreProductsBloc();
  String _url = "http://ec2-18-212-16-222.compute-1.amazonaws.com:8082/images/";

  ProductListState({this.storeId, this.storeName, this.call});

  @override
  void initState() {
    super.initState();
    storeProductsBloc.getStoreProducts(storeId.toString());
  }

  @override
  void dispose() {
    storeProductsBloc.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          storeName != null ? storeName : "Store",
          style: TextStyle(
            color: Color(0xFFF7CE3E),
            fontSize: 20,
          ),
        ),
        centerTitle: true,
        backgroundColor: Color(0xFF1A2930),
      ),
      body: Container(
        padding: EdgeInsets.only(bottom: 30, top: 5),
        child: Stack(
          children: [
            buildProducts(),
          ],
          
        ),
      ),
      bottomNavigationBar: NavBar(
        index: call,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingNav(),
    );
  }

  Widget buildProducts() {
    return StreamBuilder(
      stream: storeProductsBloc.storeProducts,
      builder: (context, AsyncSnapshot<List<CartProduct>> snapshot) {
        if (snapshot.hasData) {
          _initialize(snapshot.data.length);
          return new ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  padding: EdgeInsets.only(bottom: 5,top: 5, left: 20, right: 20),
                  child: Card(
                    color: Color(0xFF1A2930),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    elevation: 5.0,
                    child: Column(
                      children: [
                        CachedNetworkImage(
                          imageUrl: "$_url${snapshot.data[index].imgUrl}",
                          placeholder: (context, url) => CircularProgressIndicator(),
                          errorWidget: (context, url, error) => Container(
                            child: new Image.asset('assets/images/fallback.gif',fit: BoxFit.cover),
                          ),
                          fit: BoxFit.cover,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 5),
                        ),
                        ListTile(
                          title: Text(snapshot.data[index].name, style: TextStyle(color: Color(0xFFFFFFFF),fontSize: 25, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                          subtitle: Text("\$${snapshot.data[index].price}",style: TextStyle(color: Color(0xFFFFFFFF),fontSize: 22,fontStyle: FontStyle.italic), textAlign: TextAlign.center),
                        ),
                        ButtonBar(
                          alignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            FlatButton(
                              color: Color(0xFFF7CE3E),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              onPressed: () => _add(snapshot.data[index],index),
                              padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Icon(_added[index] ? Icons.playlist_add_check : Icons.playlist_add, color: Color(0xFF1A2930),size: 27,),
                                  Padding(
                                          padding: EdgeInsets.symmetric(horizontal: 5),
                                        ),
                                        Text(_added[index] ? "Remove" : "Add" , style :TextStyle(color: Color(0xFF1A2930),fontSize: 18),),
                                      ],
                                    ) ,
                                  ),
                                  FlatButton(
                                    color: Color(0xFFF7CE3E),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                    ),
                                    onPressed: () => _detailPage(snapshot.data[index], index),
                                    padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Icon( Icons.info , color: Color(0xFF1A2930),size: 27, ) ,
                                        Padding(
                                          padding: EdgeInsets.symmetric(horizontal: 5),
                                        ),
                                        Text("See More", style :TextStyle(color: Color(0xFF1A2930),fontSize: 18))
                                ],
                              ) ,
                            ),
                            
                          ],
                        ),
                        Padding(padding: EdgeInsets.symmetric(vertical: 5),)
                      ],
                    ),
                  ),
                );
              });
        } else if (!snapshot.hasData) {
          // No Products Message
        } else if (snapshot.hasError) {
          return Text(snapshot.error.toString());
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }


  _initialize(int length){
    if(_added == null){
      _added = new List<bool>(length);
      for(int i = 0; i < length; i ++){
        _added[i] = false;
      }
    }
  }
  
  _add(CartProduct product,int index){
    List<bool> isAdded = _added;
    bool add = isAdded[index];
    if(add == false){
      _repository.addProductToShoppingList(product);
    }else{
      _repository.deleteProductFromShoppingList(product.id);
    }
    isAdded[index] = !add;
    setState(() {
      _added = isAdded;
    });
  }

  _detailPage(CartProduct data, int index) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ProductDetail(
        description: data.description,
        id: data.id,
        imgUrl: data.imgUrl,
        name: data.name,
        price: data.price,
        sku: data.sku,
        storeId: data.storeId,
        amount: data.amount,
        added : _added[index],
      );
    }));
  }
}
