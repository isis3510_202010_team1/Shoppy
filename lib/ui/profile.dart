import 'package:flutter/material.dart';
import 'package:shoppy/blocs/user_bloc.dart';
import 'package:shoppy/components/floating_navigator.dart';
import 'package:shoppy/components/nav_bar.dart';
import 'package:shoppy/models/user_model.dart';

class Profile extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ProfileState();
}

class ProfileState extends State<Profile> {
  final UserBloc userBloc = new UserBloc();
  @override
  void initState() {
    userBloc.initialize();
    super.initState();
  }

  @override
  void dispose() {
    userBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Profile',
          style: TextStyle(
            color: Color(0xFFF7CE3E),
            fontSize: 27,
          ),
        ),
        centerTitle: true,
        backgroundColor: Color(0xFF1A2930),
      ),
      body: Container(
        child: ListView(
          children: [
            Stack(
              children: <Widget>[
                Container(
                  color: Color(0xFFF7CE3E),
                  height: 180.0,
                ),
                profileSection(),
                options(),
                Positioned(
                  width: 90,
                  height: 90,
                  top: 10,
                  left: 145,
                  child: Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color(0xFFFFFFFF),
                        image: new DecorationImage(
                            image: AssetImage('assets/images/user.png'),
                            fit: BoxFit.fill)),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      bottomNavigationBar: NavBar(
        index: 3,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingNav(),
    );
  }

  Widget profileSection() {
    return StreamBuilder(
      stream: userBloc.user,
      builder: (context, AsyncSnapshot<UserModel> snapshot) {
        if (snapshot.hasData) {
          return Container(
            padding: EdgeInsets.fromLTRB(10, 45, 10, 0),
            child: Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 40),
                  ),
                  ListTile(
                    title: Text(
                      snapshot.data.name,
                      style: TextStyle(
                          color: Color(
                            0xFFFFFFFF,
                          ),
                          fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                    subtitle: Text(
                      "Name",
                      style: TextStyle(color: Color(0xFFC5C1C0)),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  ListTile(
                    title: Text(
                      snapshot.data.email,
                      style: TextStyle(
                        color: Color(
                          0xFFFFFFFF,
                        ),
                        fontSize: 20,
                      ),
                    ),
                    subtitle: Text(
                      "Email",
                      style: TextStyle(color: Color(0xFFC5C1C0)),
                    ),
                  ),
                  ListTile(
                    title: Text(
                      snapshot.data.phone,
                      style: TextStyle(
                        color: Color(
                          0xFFFFFFFF,
                        ),
                        fontSize: 20,
                      ),
                    ),
                    subtitle: Text(
                      "Phone",
                      style: TextStyle(color: Color(0xFFC5C1C0)),
                    ),
                  ),
                  ListTile(
                    title: Text(
                      snapshot.data.gender,
                      style: TextStyle(
                        color: Color(
                          0xFFFFFFFF,
                        ),
                        fontSize: 20,
                      ),
                    ),
                    subtitle: Text(
                      "Gender",
                      style: TextStyle(color: Color(0xFFC5C1C0)),
                    ),
                  ),
                  ListTile(
                    title: Text(
                      "${DateTime.parse(snapshot.data.date_of_birth).day}/${DateTime.parse(snapshot.data.date_of_birth).month}/${DateTime.parse(snapshot.data.date_of_birth).year}",
                      style: TextStyle(
                        color: Color(
                          0xFFFFFFFF,
                        ),
                        fontSize: 20,
                      ),
                    ),
                    subtitle: Text(
                      "Date of Birth",
                      style: TextStyle(color: Color(0xFFC5C1C0)),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 10),
                  )
                ],
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 5.0,
              color: Color(0xFF1A2930),
            ),
          );
        } else if (snapshot.hasError) {
          return Text(snapshot.error.toString());
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }

  Widget options(){
    return Container(
      padding: EdgeInsets.fromLTRB(10,470, 10, 0),
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.receipt, color: Color(0xFFF7CE3E),),
              title: Text(
                "Past Transactions",
                style: TextStyle(
                    color: Color(
                      0xFFFFFFFF,
                    ),
                    fontSize: 20),
                textAlign: TextAlign.left,
              ),
              onTap: _goToTransactions,
            ),
            ListTile(
              leading: Icon(Icons.list, color: Color(0xFFF7CE3E),),
              title: Text(
                "Your Shopping List",
                style: TextStyle(
                    color: Color(
                      0xFFFFFFFF,
                    ),
                    fontSize: 20),
                textAlign: TextAlign.left,
              ),
              onTap: _goToShoppingList,
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10),
            )
          ],
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        elevation: 5.0,
        color: Color(0xFF1A2930),
      ),
    );
  }

  _goToTransactions(){
    Navigator.pushNamed(context, "/transactions");
  }

  _goToShoppingList(){
    Navigator.pushReplacementNamed(context, "/shoppingList");
  }
}
