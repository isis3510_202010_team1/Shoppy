import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shoppy/blocs/payment_bloc.dart';
import 'package:shoppy/components/Toast.dart';
import 'package:shoppy/models/cart_product_model.dart';
import 'package:stripe_payment/stripe_payment.dart';

class Payment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PaymentState();
  }
}

class PaymentState extends State<Payment> {
  PaymentBloc bloc = PaymentBloc();

  @override
  void initState() {
    super.initState();
    bloc.streamVirtualCartProducts();
    bloc.streamTransactionInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          title: Text("Payment",
              style: TextStyle(color: Theme.of(context).accentColor))),
      body: ListView(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              // Overview text
              Center(
                  child: Container(
                      margin: EdgeInsets.all(10),
                      child: Text("Overview",
                          style: TextStyle(
                              fontSize: 25, fontWeight: FontWeight.bold)))),
              //ProductList
              productList(),
              SizedBox(height: 10.0),
              bottomCard(),
              SizedBox(height: 10.0),
              bottomButtons(context),
            ],
          ),
        ],
      ),
    );
  }

  Widget productList() {
    return StreamBuilder(
        stream: bloc.virtualCartProducts,
        builder:
            (BuildContext context, AsyncSnapshot<List<CartProduct>> snapshot) {
          if (!snapshot.hasData || snapshot.data.length == 0) {
            print("PAYMENT: NO ITEMS");
            return Center(
              child: Text("Loading Items..."),
            );
          } else {
            print("PAYMENT: HAS ITEMS");
            return Column(
              children: <Widget>[
                Container(
                    height: MediaQuery.of(context).size.height * 0.38,
                    child: ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Center(
                          child: Column(
                            children: <Widget>[
                              Container(
                                  padding: EdgeInsets.all(10),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.4,
                                        child: Text(
                                            "${snapshot.data[index].name} \nx ${snapshot.data[index].amount}",
                                            style: TextStyle(fontSize: 20)),
                                      ),
                                      Text("\$${snapshot.data[index].price}",
                                          style: TextStyle(fontSize: 20)),
                                    ],
                                  )),
                              Divider(
                                color: Theme.of(context).primaryColor,
                                thickness: 2.0,
                              )
                            ],
                          ),
                        );
                      },
                    )),
              ],
            );
          }
        });
  }

  Widget bottomCard() {
    return StreamBuilder(
      stream: bloc.transactionInfo,
      builder:
          (BuildContext context, AsyncSnapshot<Map<String, int>> snapshot) {
        if (!snapshot.hasData) {
          return Text("Loading transaction details....");
        } else {
          return Container(
              height: MediaQuery.of(context).size.height * 0.3,
              width: MediaQuery.of(context).size.width * 0.9,
              child: Card(
                // margin: EdgeInsets.all(10),
                color: Theme.of(context).primaryColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Sub Total: ",
                              style: TextStyle(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white)),
                          Text("\$${snapshot.data['subtotal']}",
                              style: TextStyle(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white)),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Iva Payed: ",
                              style: TextStyle(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white)),
                          Text("\$${snapshot.data['iva']}",
                              style: TextStyle(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white)),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Total: ",
                              style: TextStyle(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Theme.of(context).accentColor)),
                          Text("\$${snapshot.data['total']}",
                              style: TextStyle(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Theme.of(context).accentColor)),
                        ],
                      ),
                    ],
                  ),
                ),
              ));
        }
      },
    );
  }

  Widget bottomButtons(BuildContext context) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Material(
              elevation: 5.0,
              borderRadius: BorderRadius.circular(30.0),
              color: Theme.of(context).primaryColor,
              child: MaterialButton(
                  // minWidth: MediaQuery.of(context).size.width * 0.9,
                  padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  onPressed: payNow,
                  child: Text(
                    "Pay with Credit Card",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Theme.of(context).accentColor, fontSize: 18.0),
                  ))),
        ],
      ),
    );
  }

  void payNow() async {
    if (!await bloc.isConnected()) {
      Toaster.showLongToast(
          "No internet connection detected, please try again while connected");
      return;
    }

    Navigator.pushNamed(context, '/stripe');
    //print(response);
  }
}
