import 'dart:isolate';

import 'package:background_location/background_location.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shoppy/models/store_model.dart';
import 'package:shoppy/services/location_updater.dart';
import 'package:shoppy/services/repository.dart';
import 'package:shoppy/ui/product_list.dart';
import '../components/nav_bar.dart';
import '../components/floating_navigator.dart';
import '../blocs/stores_bloc.dart';

class StoresMap extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MapState();
}

class MapState extends State<StoresMap> {
  final StoreBloc storeBloc = new StoreBloc();
  final Repository _repository = new Repository();
  LocationUpdater _locationUpdate = new LocationUpdater();


  @override
  void initState() {
    super.initState();
    storeBloc.getAllStores();
    _getStores();
    _getUserLocation();
  }

  void dispose() {
    storeBloc.dispose();
    super.dispose();
  }

  // Map related attributes
  static LatLng _initialPosition;
  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> _markers = Set();
  List<StoreModel> stores;

  // Map Related Methods
  void _onMapCreated(GoogleMapController controller) {
    if (!_controller.isCompleted) {
      _controller.complete((controller));
    }
  }

  void _getUserLocation() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);    

    Isolate.spawn(_getLocationData(),{"Nothing"});
    try {
      setState(() {
        _initialPosition = LatLng(position.latitude, position.longitude);
        _storeName = "";
        _address = "";
        _visibleButton = false;
      });
    } on Exception {
      print("Location Error");
    }
  }


  Future<void> _getStores() async {
    BitmapDescriptor mapIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), 'assets/images/marker.png');
    BitmapDescriptor mapIconDanger = await BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio:2.6), 'assets/images/marker_danger.png');
    Stream stream = storeBloc.allStores;
    Set<Marker> markers = new Set<Marker>();
    StoreModel store;
    Marker marker;
    stores = await stream.first;
    for (int i = 0; i < stores.length; i++) {
      store = stores[i];
      if(store.currentUsers != null && store.currentUsers < 40){
        marker = Marker(
          markerId: MarkerId(store.address),
          position: LatLng(store.loclat, store.loclon),
          icon: mapIcon,
          onTap: (){
            int x = i;
            _showButton(x);
          },
        );
        markers.add(marker);
      }else{
        Marker marker = Marker(
          markerId: MarkerId(store.address),
          position: LatLng(store.loclat, store.loclon),
          icon: mapIconDanger,
          onTap: (){
            int x = i;
            _showButton(x);
          },
        );
        markers.add(marker);
      }
      
      
    }
    setState(() {
      _markers.addAll(markers);
    });
  }

  // Store related attributes
  int _storeId = 0;
  String _storeName = "";
  String _address = "";
  int _currentusers = 0;
  bool _visibleButton = false;
  bool _tooManyPeople = false;

  // Store info and products related Methods
  _showButton(int i) {
    _repository.saveStoreId(stores[i].id.toString());
    setState(() {
      _storeId = stores[i].id;
      _storeName = stores[i].name;
      _address = stores[i].address;
      _visibleButton = true;
      _currentusers = stores[i].currentUsers;
      if(stores[i].currentUsers >40){
        _tooManyPeople = true;
      }else{
        _tooManyPeople = false;
      }
    });
  }
  _goToStore(){
    Navigator.push(context, MaterialPageRoute(builder: (context){
      return ProductList(
        storeId: _storeId,
        storeName: _storeName,
        call: 0,
      );
    })
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Shoppy',
          style: TextStyle(
            color: Color(0xFFF7CE3E),
            fontSize: 27,
          ),
        ),
        centerTitle: true,
        backgroundColor: Color(0xFF1A2930),
      ),
      body: _initialPosition == null
          ? Container(
              child: GoogleMap(
              onMapCreated: _onMapCreated,
              initialCameraPosition: CameraPosition(
                target: LatLng(4.6097102, -74.081749),
                zoom: 10,
              ),
              myLocationButtonEnabled: false,
              myLocationEnabled: false,
              markers: _markers,
            ))
          : Stack(
              children: <Widget>[
                GoogleMap(
                  onMapCreated: _onMapCreated,
                  initialCameraPosition: CameraPosition(
                    target: _initialPosition,
                    zoom: 15,
                  ),
                  myLocationButtonEnabled: true,
                  myLocationEnabled: true,
                  markers: _markers,
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  alignment: AlignmentDirectional.bottomCenter,
                  margin: EdgeInsets.only(bottom: 18),
                  child: Visibility(
                    child: Card(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: _tooManyPeople? Icon(Icons.report_problem, color:Color(0xFFF32013), size: 50,):Icon(Icons.store, color:Color(0xFFF7CE3E), size: 50,) ,
                            title: Text(_storeName, style: TextStyle(color: Color(0xFFFFFFFF)),),
                            subtitle: _tooManyPeople? Text("$_address. Be careful there are a lot of people in the store",style: TextStyle(color: Color(0xFFFFFFFF),fontStyle: FontStyle.italic)):Text(_address,style: TextStyle(color: Color(0xFFFFFFFF))),
                          ),
                          FlatButton(
                            color: Color(0xFFF7CE3E),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            onPressed: _goToStore,
                            padding: EdgeInsets.symmetric(vertical: 10,horizontal: 30),
                            child: Text('Explore Store',style: TextStyle(color: Color(0xFF1A2930),fontSize: 18)),
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 15),
                          )
                        ],
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      elevation: 10.0,
                      color: Color(0xFF1A2930),
                    ),
                    visible: _visibleButton,
                  ),
                ),
              ],
            ),
      bottomNavigationBar: NavBar(
        index: 0,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingNav(),
    );
  }

  _getLocationData(){
    BackgroundLocation.startLocationService();
    Timer timer = Timer.periodic(Duration(minutes: 1), (Timer t) {
      BackgroundLocation.getLocationUpdates((location) {
        _locationUpdate.reportLocation(location.latitude, location.longitude);
      });
    });
  }

}
