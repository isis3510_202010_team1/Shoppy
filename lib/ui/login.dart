import 'package:flutter/material.dart';
import 'package:shoppy/components/Toast.dart';
import '../blocs/login_bloc.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<Login> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final loginBloc = LoginBloc();

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: ListView(
      shrinkWrap: true,
      children: <Widget>[
        Container(
            color: Colors.white,
            child: Padding(
                padding: const EdgeInsets.all(36.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 200.0,
                      child: Image.asset("assets/images/iconName.png",
                          fit: BoxFit.contain),
                    ),
                    // Email Field
                    SizedBox(height: 40.0),
                    TextField(
                        controller: emailController,
                        obscureText: false,
                        // style: style,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            hintText: "Email",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(32.0)))),
                    // Password Field
                    SizedBox(height: 20.0),
                    TextField(
                      controller: passwordController,
                      obscureText: true,
                      // style: style,
                      decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                          hintText: "Password",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0))),
                    ),
                    SizedBox(height: 20.0),
                    Material(
                        elevation: 5.0,
                        borderRadius: BorderRadius.circular(30.0),
                        color: Color(0xFFF7CE3E),
                        child: MaterialButton(
                            minWidth: MediaQuery.of(context).size.width,
                            padding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            onPressed: loginPress,
                            child: Text(
                              "Login",
                              textAlign: TextAlign.center,
                              // style: style.copyWith)
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor, fontSize: 18.0),
                            ))),
                    SizedBox(height: 20.0),
                    Material(
                        elevation: 5.0,
                        borderRadius: BorderRadius.circular(30.0),
                        color: Color(0xFF0A1612),
                        child: MaterialButton(
                            minWidth: MediaQuery.of(context).size.width,
                            padding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            onPressed: signInPress,
                            child: Text(
                              "Create Account",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Color(0xFFC5C1C0), fontSize: 18.0),
                            ))),
                  ],
                )))
      ],
    )));
  }

  loginPress() async {
    if (!await loginBloc.isConnected()) {
      Toaster.showLongToast("Login Error: No Internet Connection");
      return;
    }
    bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(emailController.text);
    if(!emailValid){
      Toaster.showLongToast("The email is invalid, please enter a valid email");
      return;
    }
    if(emailController.text.length > 100){
      Toaster.showLongToast("The email is too long, please enter a valid email");
      return;
    }
    if(passwordController.text.length > 100){
      Toaster.showLongToast("The password is too long, please enter a valid password (less than 100 characters)");
      return;
    }
    String response = await loginBloc.login(
        emailController.text.trim(), passwordController.text.trim());
    if (response.contains("Failed")) {
      print("wrong credentials");
      Toaster.showLongToast(
          "Login Error: Wrong email or password, please try again");
    } else {
      print(response);
      loginBloc.saveUserId(response);
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/storesMap', (Route<dynamic> route) => false);
    }
  }

  signInPress() {
    print("signIn");
    Navigator.pushNamed(context, "/createAccount");
  }
}
